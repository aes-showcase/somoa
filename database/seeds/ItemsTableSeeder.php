<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("items")->delete();
        \DB::table("items")->insert([
            [
                "id" => 1,
                "name" => "Hotdog",
                "category_id" => 1,
                "price" => 30,
                "tax" => 3.6,
                "image" => "images/hotdog.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 2,
                "name" => "CheeseBurger",
                "category_id" => 1,
                "price" => 50,
                "tax" => 6,
                "image" => "images/cheeseburger.jpg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 3,
                "name" => "Fries",
                "category_id" => 1,
                "price" => 40,
                "tax" => 4.8,
                "image" => "images/fries.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 4,
                "name" => "Coke",
                "category_id" => 2,
                "price" => 55,
                "tax" => 6.6,
                "image" => "images/coke.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 5,
                "name" => "Sprite",
                "category_id" => 2,
                "price" => 45,
                "tax" => 5.4,
                "image" => "images/sprite.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 6,
                "name" => "Tea",
                "category_id" => 2,
                "price" => 99,
                "tax" => 11.88,
                "image" => null,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 7,
                "name" => "Chicken Combo Meal",
                "category_id" => 3,
                "price" => 122,
                "tax" => 14.64,
                "image" => "images/chicken.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 8,
                "name" => "Pork Combo Meal",
                "category_id" => 3,
                "price" => 144,
                "tax" => 17.28,
                "image" => "images/pork.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 9,
                "name" => "Fish Combo Meal",
                "category_id" => 3,
                "price" => 150,
                "tax" => 18,
                "image" => "images/fish.jpeg",
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 10,
                "name" => "Coffee",
                "category_id" => 2,
                "price" => 5,
                "tax" => 0.6,
                "image" => null,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
        ]);
    }
}
