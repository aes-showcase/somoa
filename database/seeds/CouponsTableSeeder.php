<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CouponsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("coupons")->delete();
        \DB::table("coupons")->insert([
            [
                "id" => 1,
                "code" => "GO2019",
                "discount" => 0.10,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 2,
                "code" => "SOMOA99",
                "discount" => 0.99,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
        ]);
    }
}
