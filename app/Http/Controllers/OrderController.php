<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\Item;
use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "quantity" => "required",
        ]);

        $itemsOrdered = array_where($request->quantity, function ($quantity, $id) {
            return $quantity > 0;
        });

        if (count($itemsOrdered) == 0) {
            $request->session()->flash("quantity", "Select at least one item");
            return back()->withInput();
        }

        $coupon = null;
        if ($request->has("coupon") && $request->coupon) {
            $coupon = Coupon::whereCode($request->coupon)->first();
            if (!$coupon) {
                $request->session()->flash("coupon", "Coupon code is invalid");
                return back()->withInput();
            }
        }

        // Create the order
        $order = \DB::transaction(function () use($coupon, $itemsOrdered) {
            $order = Order::create([
                "user_id" => Auth::check() ? Auth::user()->id : 1,
                "coupon_id" => $coupon ? $coupon->id: null,
            ]);

            foreach($itemsOrdered as $id => $quantity) {
                $item = Item::findOrFail($id);
                $price = $item->price;
                $tax = $item->tax;
                if ($coupon) {
                    $price = $price - ($price * $coupon->discount);
                    $tax = $tax - ($tax * $coupon->discount);
                }
                $order->items()->attach($id, [
                    "price" => $price,
                    "tax" => $tax,
                    "quantity" => $quantity,
                ]);
            }
            return $order;
        });

        $summary = self::computeSummary($order);

        $request->session()->flash("order", "Thank you for your order!");
        return view("store.order", compact("order", "summary"));
    }

    private function computeSummary($order) {
        $summary = [
            "price" => 0,
            "tax" => 0,
            "due" => 0,
        ];

        foreach($order->items as $item) {
            $summary["price"] += $item->pivot->price;
            $summary["tax"] += $item->pivot->tax;
            $summary["due"] += $item->pivot->price + $item->pivot->tax;
        }

        return $summary;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        if (Auth::user()->id == $order->user->id) {
            $summary = self::computeSummary($order);
            return view("store.order", compact("order", "summary"));
        } else {
            return redirect(route("store"));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function computeOrder(Request $request) {
        $itemsOrdered = array_where($request->items, function ($item, $key) {
            return $item["quantity"] > 0;
        });

        if (count($itemsOrdered) == 0) {
            return null;
        }

        $items = [];
        foreach($itemsOrdered as $key => $itemOrdered) {
            $item = Item::findOrFail($itemOrdered["id"]);
            $items[] = [
                "id" => $item->id,
                "name" => $item->name,
                "quantity" => $itemOrdered["quantity"],
                "price" => $item->price,
                "tax" => $item->tax,
            ];
        }

        $coupon = null;
        if ($request->has("coupon") && $request->coupon) {
            $coupon = Coupon::whereCode($request->coupon)->first();
            if (!$coupon) {
                return [
                    "error" => "Coupon code is invalid",
                ];
            }
        }

        $result = [
            "items" => $items,
            "discount" => $coupon ? $coupon->discount : null,
        ];

        return $result;
    }
}
