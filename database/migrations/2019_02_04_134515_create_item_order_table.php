<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_order', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger("order_id");
            $table->foreign("order_id")
                ->references("id")
                ->on("orders")
                ->onDelete("restrict")
                ->onUpdate("cascade");
            $table->unsignedInteger("item_id");
            $table->foreign("item_id")
                ->references("id")
                ->on("items")
                ->onDelete("restrict")
                ->onUpdate("cascade");
            $table->decimal("price", 10, 2);
            $table->decimal("tax", 10, 2);
            $table->integer("quantity");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_order');
    }
}
