<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        "user_id",
        "coupon_id",
    ];

    public function user () {
        return $this->belongsTo("App\User");
    }

    public function coupon () {
        return $this->belongsTo("App\Coupon")->withDefault();
    }

    public function items () {
        return $this->belongsToMany("App\Item")
            ->withPivot([
                "price",
                "tax",
                "quantity",
            ])
            ->withTimestamps();
    }
}
