<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->text("name");
            $table->unsignedInteger("category_id");
            $table->foreign("category_id")
                ->references("id")
                ->on("categories")
                ->onDelete("restrict")
                ->onUpdate("cascade");
            $table->decimal("price", 10, 2);
            $table->decimal("tax", 10, 2);
            $table->string("image")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
