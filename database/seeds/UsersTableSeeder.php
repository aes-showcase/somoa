<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("users")->delete();
        \DB::table("users")->insert([
            [
                "id" => 1,
                "name" => "user",
                "email" => "user@user.com",
                "password" => bcrypt("useruser"),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 2,
                "name" => "user1",
                "email" => "user1@user.com",
                "password" => bcrypt("user1user1"),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 3,
                "name" => "user2",
                "email" => "user2@user.com",
                "password" => bcrypt("user2user2"),
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
        ]);
    }
}
