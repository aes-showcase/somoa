@extends('layouts.app')

@section('content')
    <div class="container bg-white py-5">
        <div class="row">
            <div class="d-none d-md-block col-md-3 border-right">
                <h4 class="text-center">Categories</h4>
                <div class="border-top">&nbsp;</div>
                <ul class="nav flex-column nav-pills">
                    @foreach($categories as $index => $category)
                        <li class="nav-item nav-item-category cursor-pointer">
                            <span class="nav-link {{ $index == 0 ? 'active' : '' }}">{{ $category->name }}</span>
                        </li>
                    @endforeach
                    <div class="border-bottom">&nbsp;</div>
                    <li class="nav-item mt-3">
                        <input type="text" placeholder="Coupon Code" class="order-coupon form-control {{ Session::has("coupon") ? 'is-invalid' : '' }}"
                            onchange="document.getElementById('coupon').value = this.value" value="{{ old('coupon') }}">
                        <div>
                            <span class="text-danger">{{ Session::get("coupon") }}</span>
                        </div>
                    </li>
                    <li class="nav-item mt-3">
                        @auth
                            <button class="btn btn-success btn-block order-button">Order</button>
                        @else
                            <a class="btn btn-success btn-block" href="{{ route('login') }}">Order</a>
                        @endauth
                    </li>
                </ul>
            </div>
            <div class="col-12 col-md-9">
                <div class="row d-md-none mb-3 pb-3 border-bottom">
                    <div class="col-12 col-md-9 pb-3 d-flex justify-content-center align-items-center">
                        <ul class="nav nav-pills">
                            @foreach($categories as $index => $category)
                                <li class="nav-item nav-item-category-mobile cursor-pointer">
                                    <span class="nav-link {{ $index == 0 ? 'active' : '' }}">{{ $category->name }}</span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-12 col-md-3">
                        <div>
                            <input type="text" placeholder="Coupon Code" class="order-coupon form-control {{ Session::has("coupon") ? 'is-invalid' : '' }}"
                                onchange="document.getElementById('coupon').value = this.value" value="{{ old('coupon') }}">
                            <div>
                                <span class="text-danger">{{ Session::get("coupon") }}</span>
                            </div>
                        </div>
                        <div class="mt-3">
                            @auth
                                <button class="btn btn-success btn-block order-button">Order</button>
                            @else
                                <a class="btn btn-success btn-block" href="{{ route('login') }}">Order</a>
                            @endauth
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        @auth
                        <form id="order-form" method="post" action="{{ route("order.store") }}" style="">
                        @else
                        <form id="order-form" method="get" action="{{ route("login") }}" style="">
                        @endauth
                            @csrf
                            <input id="coupon" name="coupon" type="text" style="display: none;" value="{{ old('coupon') }}">
                            <h2 class="main-title display-1 text-center mb-0">Menu</h2>
                            <div id="alert-quantity" class="alert {{ Session::has("quantity") ? 'alert-danger' : '' }}">
                                <span>{{ Session::get("quantity") }}</span>
                            </div>
                            @foreach($categories as $index => $category)
                                <div class="row category-items {{ $index != 0 ? 'd-none' : '' }}">
                                    @foreach($category->items as $item)
                                        <div class="col-12 col-md-4 mb-3">
                                            <div class="card">
                                                @if($item->image)
                                                    <img src="{{ asset($item->image) }}" class="card-img-top img-thumbnail img-contain skeleton">
                                                @else
                                                    <div class="d-flex justify-content-center align-items-center icon-contain img-thumbnail">
                                                        <i class="fas fa-image fa-7x"></i>
                                                    </div>
                                                @endif
                                                <div class="card-body d-flex flex-column">
                                                    <h5 class="card-title">{{ $item->name }}</h5>
                                                    <p class="card-text">
                                                        PHP {{ money_format("%i" , $item->price + $item->tax) }} (VAT inclusive)
                                                    </p>
                                                    <div class="flex-grow-1">
                                                        &nbsp;
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Quantity</label>
                                                        <input id="item-{{ $item->id }}" type="number" name="quantity[{{ $item->id }}]" min="0"
                                                            class="item-quantity form-control {{ Session::has("quantity") ? 'is-invalid' : ''}}" value="{{ old('quantity')[$item->id] }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 id="modal-title" class="modal-title">Confirm Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <ul id="order-items" class="list-group"></ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card mt-3">
                                <div class="card-body">
                                    <h5 class="card-title d-flex justify-content-between">Total: <span id="order-total"></span></h5>
                                    <p class="card-text d-flex justify-content-between">Discount: <span id="order-discount"></span></p>
                                    <div class="dropdown-divider"></div>
                                    <h5 class="card-title d-flex justify-content-between">Due: <span id="order-due"></span></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Change</button>
                    <button id="order-submit" type="button" class="btn btn-primary">Order</button>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ secure_asset("js/store.js") }}" type="application/javascript"></script>
@endsection
