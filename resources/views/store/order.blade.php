@extends('layouts.app')

@section('content')
    <div class="container bg-white py-5">
        <div class="row">
            <div class="col">
                @if(Session::has("order"))
                    <div class="alert alert-success">
                        {{ Session::get("order") }}
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title text-center">Order Details:</h2>
                        <div class="row">
                            <div class="col table-responsive">
                                <table class="table table-striped my-3">
                                    <thead>
                                        <th>Order ID</th>
                                        <th>Order Date</th>
                                        <th>Coupon</th>
                                        <th>Total Price</th>
                                        <th>Total Tax</th>
                                        <th>Total Due</th>
                                    </thead>
                                    <tbody>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ date("d-m-Y", strtotime($order->created_at)) }}</td>
                                        <td>{{ $order->coupon->code }}</td>
                                        <td>PHP {{ money_format("%i", $summary["price"]) }}</td>
                                        <td>PHP {{ money_format("%i", $summary["tax"]) }}</td>
                                        <td class="font-weight-bold">PHP {{ money_format("%i", $summary["due"]) }}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="row border-bottom">
                            @foreach($order->items as $item)
                                <div class="col-12 col-md-4 mb-3">
                                    <div class="card h-100">
                                        @if($item->image)
                                            <img src="{{ secure_asset($item->image) }}" class="card-img-top img-thumbnail img-contain skeleton">
                                        @else
                                            <div class="d-flex justify-content-center align-items-center icon-contain img-thumbnail">
                                                <i class="fas fa-image fa-7x"></i>
                                            </div>
                                        @endif
                                        <div class="card-body text-center">
                                            <h3 class="card-title">{{ $item->name }} <span class="badge badge-success">{{ $item->pivot->quantity }}</span></h3>
                                            <h5 class="card-title mb-0 order__total">PHP {{ money_format("%i" , $item->pivot->price + $item->pivot->tax) }}</h5>
                                            <small class="card-text order_details">
                                                PHP {{ money_format("%i", $item->pivot->price) }} + {{ money_format("%i", $item->pivot->tax) }} VAT
                                            </small>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="mt-3 d-flex justify-content-between">
                            <a href="{{ route("home") }}" class="btn btn-light">
                                Back to Home
                            </a>
                            <a href="{{ route("store") }}" class="btn btn-primary">
                                Back to Store
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
