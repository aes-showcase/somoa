const categoryItems = document.querySelectorAll(".category-items")

const categoryLinks = document.querySelectorAll(".nav-item-category")
Array.from(categoryLinks).forEach((category, index) => {
    category.addEventListener("click", () => {
        categoryLinks.forEach((category) => {
            category.querySelector("span").classList.remove("active")
        })
        category.querySelector("span").classList.toggle("active")
        Array.from(categoryItems).forEach((item) => {
            item.classList.add("d-none")
        })
        categoryItems[index].classList.remove("d-none")
    })
})

const categoryLinksMobile = document.querySelectorAll(".nav-item-category-mobile")
Array.from(categoryLinksMobile).forEach((category, index) => {
    category.addEventListener("click", () => {
        categoryLinksMobile.forEach((category) => {
            category.querySelector("span").classList.remove("active")
        })
        category.querySelector("span").classList.toggle("active")
        Array.from(categoryItems).forEach((item) => {
            item.classList.add("d-none")
        })
        categoryItems[index].classList.remove("d-none")
    })
})

const orderButtons = document.querySelectorAll(".order-button")
Array.from(orderButtons).forEach((orderButton) => {
    orderButton.addEventListener("click", async () => {
        const coupon = document.querySelector("#coupon").value
        let items = document.querySelectorAll(".item-quantity")
        items = Array.from(items).map((item) => ({
            id: item.id.split("-")[1],
            quantity: item.value,
        }))

        // Validate items
        if (items.filter((item) => item.quantity > 0).length === 0) {
            const alertQuantity = document.querySelector("#alert-quantity")
            alertQuantity.classList.add("alert-danger")
            alertQuantity.textContent = "Select at least one item"
            return
        }

        // Send item ordered and quantity
        const result = await axios.post("/order/compute", {
            items,
            coupon,
        })

        // Receive price / tax / discount amounts
        const orderCoupons = document.querySelectorAll(".order-coupon")
        if (result.data.error) {
            Array.from(orderCoupons).forEach((orderCoupon) => {
                orderCoupon.classList.add("is-invalid")
                orderCoupon.nextElementSibling.firstChild.textContent = result.data.error
            })
            return
        } else {
            Array.from(orderCoupons).forEach((orderCoupon) => {
                orderCoupon.classList.remove("is-invalid")
                orderCoupon.nextElementSibling.firstChild.textContent = ""
            })
        }

        if (result.data) {
            result.data.items = result.data.items.map((item) => {
                item.price = Number(item.price)
                item.tax = Number(item.tax)
                item.quantity = Number(item.quantity)
                return item
            })
            if (result.data.discount) {
                result.data.discount = Number(result.data.discount)
            }
            console.log(1, result.data)
            renderItems(result.data)
        } else {
            return
        }

        // Post to order.store

    })
})

const orderForm = document.querySelector("#order-form")
const orderSubmit = document.querySelector("#order-submit")
orderSubmit.addEventListener("click", () => {
    orderForm.submit()
})

const renderItems = (data) => {
    const currencyOptions = {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
    }

    const orderItems = document.querySelector("#order-items")
    while(orderItems.firstChild) {
        orderItems.removeChild(orderItems.firstChild)
    }

    data.items.forEach((item) => {
        const listItem = document.createElement("li")
        listItem.classList.add("list-group-item", "d-flex", "justify-content-between")

        const listName = document.createElement("span")
        listName.textContent = item.name + " - "
        const listQuantity = document.createElement("span")
        listQuantity.classList.add("badge", "badge-primary")
        listQuantity.textContent = item.quantity
        listName.appendChild(listQuantity)
        listItem.appendChild(listName)

        const listPrice = document.createElement("span")
        const subtotal = (item.price + item.tax) * item.quantity
        listPrice.textContent = `PHP ${item.price} + ${item.tax} VAT = ${subtotal.toLocaleString("en-PH", currencyOptions)}`
        listItem.appendChild(listPrice)

        orderItems.appendChild(listItem)
    })

    const total = data.items.reduce((total, item) => {
        return total + ((item.price + item.tax) * item.quantity)
    }, 0)
    const orderTotal = document.querySelector("#order-total")
    orderTotal.textContent = `PHP ${total.toLocaleString("en-PH", currencyOptions)}`

    const orderDiscount = document.querySelector("#order-discount")
    if (data.discount) {
        orderDiscount.textContent = (data.discount * 100) + "% - " + ((total * data.discount).toLocaleString("en-PH", currencyOptions))
        orderDiscount.parentElement.classList.remove("text-secondary")
        orderDiscount.parentElement.classList.add("text-success", "font-weight-bold")
    } else {
        orderDiscount.textContent = 0
        orderDiscount.parentElement.classList.remove("text-success", "font-weight-bold")
        orderDiscount.parentElement.classList.add("text-secondary")
    }

    const orderDue = document.querySelector("#order-due")
    const due = total - (total * (data.discount ? data.discount : 0))
    orderDue.textContent = `PHP ${due.toLocaleString("en-PH", currencyOptions)}`
    
    $('#modal-confirm').modal()
}
