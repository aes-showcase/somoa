@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center">
                    Orders
                    <a class="btn btn-primary" href="{{ route("store") }}">Go to Store</a>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                        @if(count(Auth::user()->orders) > 0)
                            @foreach(Auth::user()->orders as $order)
                                <div class="col-12 col-md-4 mb-3">
                                    <a href="{{ route("order.show", $order->id) }}" class="reset-link">
                                        <div class="card h-100 home__order">
                                            <div class="card-body text-center">
                                                    <h5 class="card-title">Order# {{ $order->id }}</h5>
                                                    <ul class="list-group">
                                                        @foreach($order->items as $item)
                                                            <li class="list-group-item list-group-item-primary">
                                                                {{ $item->name }}
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="col">
                                <a href="{{ route("store") }}">Start ordering now!</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
