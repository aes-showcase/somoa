<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ItemOrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("item_order")->delete();
        \DB::table("item_order")->insert([
            [
                "id" => 1,
                "order_id" => 1,
                "item_id" => 1,
                "price" => 30,
                "tax" => 3.6,
                "quantity" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 2,
                "order_id" => 1,
                "item_id" => 2,
                "price" => 50,
                "tax" => 6,
                "quantity" => 3,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 3,
                "order_id" => 2,
                "item_id" => 4,
                "price" => 55,
                "tax" => 6.6,
                "quantity" => 1,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 4,
                "order_id" => 3,
                "item_id" => 7,
                "price" => 122,
                "tax" => 14.64,
                "quantity" => 5,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
            [
                "id" => 5,
                "order_id" => 3,
                "item_id" => 9,
                "price" => 150,
                "tax" => 18,
                "quantity" => 5,
                "created_at" => Carbon::now(),
                "updated_at" => Carbon::now(),
            ],
        ]);
    }
}
