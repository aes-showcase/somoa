<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("store", "ItemController@index")->name("store");
Route::resource("item", "ItemController");
Route::middleware("auth")->group(function () {
    Route::resource("order", "OrderController");
    Route::post("order/compute", "OrderController@computeOrder")->name("order.compute");
});

Route::get("/.well-known/acme-challenge/oTJciIAPt2mOcRjcJqG0GikbvfdyYRuPE94B5ZoNY-c", function () {
    return "oTJciIAPt2mOcRjcJqG0GikbvfdyYRuPE94B5ZoNY-c.ebuo2KIon8xkwLbJbGWw5EhnHMxJUl_8a_VrOjBJfjg";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
