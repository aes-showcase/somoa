<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Somoa</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">

        <!-- Styles -->
        <style>
            html, body {
                /*background-color: #fff;*/
                /*color: #636b6f;*/
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
                background-image: linear-gradient(to right bottom, rgba(223, 41, 53, 1) 0%, rgba(223, 41, 53, 0.5) 75%, rgba(255,255,255,0.5) 99%);
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .top-left {
                position: absolute;
                left: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 42px;
            }

            .links > a {
                /*color: #636b6f;*/
                color: #fff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .cta {
                color: #fff!important;
                background-color: #3772ff;
                padding: 10px 20px!important;
                display: inline-block;

                background-image: linear-gradient(120deg, #3772ff 0%, #3772ff 50%, #fff 50%);
                background-size: 250%;
                transition: all 0.3s ease-out;
            }

            .cta:hover {
                color: #000!important;
                background-position: 100%;
                transform: scale(1.05);
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-left links">
                <a href="/">Somoa</a>
            </div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Somoa
                </div>
                <div class="subtitle m-b-md">
                    A Simple Online Menu Ordering Application
                </div>

                <div class="links">
                    <a href="{{ route("store") }}" class="cta">Order Now!</a>
                </div>
            </div>
        </div>
    </body>
</html>
